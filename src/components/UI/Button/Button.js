import React from "react";
import classes from "./Button.module.css";

const button = props => (
  <button
    className={[classes.Button, classes[props.btnType]].join(" ")} //po cholere ta tablica? dlaczego nie mozna dac dwóch klas?
    onClick={props.clicked}
  >
    {props.children}
  </button>
);

export default button;
